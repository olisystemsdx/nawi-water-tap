import requests
import json
import time
import pandas as pd
import pymongo
import copy 

df = pd.read_csv(r'C:\Users\adithya.bannady\OneDrive - OLI Systems\OLI\Solutions\NAWI - Water TAP\Shared by customer\seawater_analysis.csv')


class OLIApi:
    '''
    A class to wrap OLI Cloud API calls to be accessible in a simple manner. This
    is just an example
    '''
    def __init__(self, username, password):
        '''
        Constructs all necessary attributes for OLIApi class

        username: user's username
        password: user's password
        '''
        self.__username = "user"
        self.__password = "password"
        self.__jwt_token = ""
        self.__refresh_token = ""
        self.__root_url = "https://api.olisystems.com"
        self.__auth_url = "https://auth.olisystems.com/auth/realms/api/protocol/openid-connect/token"
        self.__dbs_url = self.__root_url + "/channel/dbs"
        self.__upload_dbs_url = self.__root_url + "/channel/upload/dbs"

    def login(self):
        '''
        Login into user credentials for the OLI Cloud and returns:
        :return: True on success, False on failure
        '''

        headers = {
            "Content-Type": "application/x-www-form-urlencoded"
        }

        body = {
            "username": self.__username,
            "password": self.__password,
            "grant_type": "password",
            "client_id": "apiclient",
        }

        req_result = requests.post(self.__auth_url, headers=headers, data=body)
        print(f'status code is {req_result.status_code}')

        if req_result.status_code == 200:
            print(f'status code is {req_result.status_code}')
            req_result = req_result.json()
            if "access_token" in req_result:
                self.__jwt_token = req_result["access_token"]
                if "refresh_token" in req_result:
                    self.__refresh_token = req_result["refresh_token"]
                    return True

        return False

    def refresh_token(self):
        '''
        Refreshes the access token using the reresh token got obtained on login and returns:
        :return: True on success, False on failure
        '''

        headers = {
            "Content-Type": "application/x-www-form-urlencoded"
        }

        body = {
            "refresh_token": self.__refresh_token,
            "grant_type": "refresh_token",
            "client_id": "apiclient",
        }

        req_result = requests.post(self.__auth_url, headers=headers, data=body)
        if req_result.status_code == 200:
            req_result = req_result.json()
            if bool(req_result):
                if "access_token" in req_result:
                    self.__jwt_token = req_result["access_token"]
                    if "refresh_token" in req_result:
                        self.__refresh_token = req_result["refresh_token"]
                        return True

        return False

    def request_auto_login(self, req_func):
        '''
        Gets a new access token if the request returns with an expired token error. First tries with the refresh token
        if its still active or simple relogs in using the username and password.

        :param req_func: function to call
        :return: Returns an empty dict if failed
        '''

        num_tries = 1
        while num_tries <= 2:

            headers = {
                "authorization": "Bearer " + self.__jwt_token
            }

            req_result = req_func(headers)
            if req_result.status_code == 200:
                ret_val = json.loads(req_result.text)
                return ret_val
            elif num_tries == 1 and req_result.status_code == 401:
                req_result = req_result.json()
                if not self.refresh_token():
                    if not self.login():
                         break
            else:
                break
            num_tries = num_tries + 1

        return dict()

    def upload_dbs_file(self, file_path):
        '''
        Uploads a dbs file to the OLI Cloud given a full file path.

        :param file_path: full path to dbs file
        :return: dictionary containing the
        uploaded file id
        '''
        req_result = dict()

        # read the file data in
        try:
            with open(file_path, "rb") as file:
                files = {"files": file}

                req_result = self.request_auto_login(lambda headers: requests.post(self.__upload_dbs_url, headers=headers,
                                               files=files))
        except IOError:
            pass

        return req_result

    def get_user_dbs_files(self):
        '''
        Returns a dictionary containing a list of dbs file(s) uploaded

        :return: dictionary containing list of dbs files
        '''
        return self.request_auto_login(
            lambda headers: requests.get(self.__dbs_url, headers=headers))

    def call(self, function_name, chemistry_model_file_id, json_input = dict(), poll_time = 1.0, max_request = 1000):
        '''
        calls a function in the OLI Engine API.

        :param function_name: name of function to call
        :param chemistry_model_file_id: the chemistry model file if for this calculation
        :param json_input: calculation input JSON
        :param poll_time: max delay between each call
        :param max_request: maximum requests
        :return: dictionary containing result or error
        '''

        # formulate url
        endpoint = ""
        method = "POST"
        if function_name == "chemistry-info" or function_name == "corrosion-contact-surface":
            endpoint = self.__root_url + "/engine/file/" + chemistry_model_file_id + "/" + function_name
            method = "GET"
        else:
            endpoint = self.__root_url + "/engine/flash/" + chemistry_model_file_id + "/" + function_name
            method = "POST"

        # http body
        if bool(json_input):
            data = json.dumps(json_input)
        else:
            data = ""

        def add_additional_header(headers):
            headers["content-type"] = "application/json"
            if method == "POST":
                return requests.post(endpoint, headers=headers, data=data)

            output = requests.get(endpoint, headers=headers, data=data)
            with open("testnnnn.text", 'w') as outfile:
                outfile.write(str(output.text))
            return output

        #first call
        results_link = ""
        start_time = time.time()
        request_result1 = self.request_auto_login(add_additional_header)
        end_time = time.time()
        request_time = end_time - start_time
        print("First request time =", request_time)
        if bool(request_result1):
            if request_result1["status"] == "SUCCESS":
                if "data" in request_result1:
                    if "status" in request_result1["data"]:
                        if request_result1["data"]["status"] == "IN QUEUE" or request_result1["data"]["status"] == "IN PROGRESS":
                            if "resultsLink" in request_result1["data"]:
                                results_link = request_result1["data"]["resultsLink"]

        print(results_link)

        # error in getting results link
        if results_link == "":
            return dict()

        # poll on results link until success
        data = ""
        endpoint = results_link
        method = "GET"
        request_iter = 0
        while True:
            # make request and time
            start_time = time.time()
            request_result2 = self.request_auto_login(add_additional_header)
            end_time = time.time()
            request_time = end_time - start_time
            print("Second request time =", request_time)

            # check if max requests exceeded
            request_iter = request_iter + 1
            if request_iter > max_request:
                break

            # extract
            print(request_result2)
            if bool(request_result2):
                if "status" in request_result2:
                    status = request_result2["status"]
                    print(status)
                    if status == "PROCESSED" or status == "FAILED":
                        if "data" in request_result2:
                            return request_result2["data"]
                        else:
                            break
                    elif status == "IN QUEUE" or status == "IN PROGRESS":
                        if poll_time > request_time:
                            time.sleep(poll_time - request_time)
                        continue
                    else:
                        break
                else:
                    break
            else:
                break

        return dict()

# example program (Isothermal flash)
if __name__ == "__main__":
    oliapi = OLIApi("username", "password")
    if oliapi.login():

        # upload chemistry file (this needs to be done only once to get the file id)
        # this needs to be only done once per chemistry model file
        # after that the id is sufficient
        #result = oliapi.upload_dbs_file(r"C:\Users\adama\OneDrive\Documents\My OLI Cases\OLI Studio 11.0\Brine.dbs")
        #print(json.dumps(result, indent=2))

        # chemistry_file_id = result["file"][0]["id"]
        chemistry_file_id = '0bd62e1b-3b47-40e8-b3f3-7b8f999d8b08' #chembuilder file '5af0d6c5-aed4-4c57-85f4-d088adba24d3' #Old DBS from Studio : 
        # print("result file:", result["file"])

        #chemistry file ID needs to be saved by the user
        print(chemistry_file_id)
        #
        # # display all available dbs files
        #result = oliapi.get_user_dbs_files()
        #print(json.dumps(result, indent=2))
        #
        # # get chemistry information
        #result = oliapi.call("chemistry-info", chemistry_file_id)
        #print(json.dumps(result, indent=2))
        #
        # import pandas as pd
        # data = pd.DataFrame()
        # row = []
        SI=[]
        for row in range(df.shape[0]):
            # create water analysis function
            brine_input = {
                "params": {
                    "waterAnalysisInputs": [
                        {
                            "group": "Cations",
                            "name": "NAION",
                            "unit": "mg/L",
                            "value": 45501.0, #df['Na'][row],
                            "charge": 1
                        },
                        {
                            "group": "Cations",
                            "name": "MGION",
                            "unit": "mg/L",
                            "value": 5703.00, #df['Mg'][row],
                            "charge": 2
                        },
                        {
                            "group": "Cations",
                            "name": "CAION",
                            "unit": "mg/L",
                            "value": 1563.00, #df['Ca'][row],
                            "charge": 2
                        },
                        {
                            "group": "Anions",
                            "name": "CLION",
                            "unit": "mg/L",
                            "value": 83049.0, #df['Cl'][row],
                            "charge": -1
                        },
                        {
                            "group": "Anions",
                            "name": "SO4ION",
                            "unit": "mg/L",
                            "value": 8739.00, #df['SO4'][row],
                            "charge": -2
                        },
                        {
                            "group": "Properties",
                            "name": "Temperature",
                            "unit": "°C",
                            "value": 25
                        },
                        {
                            "group": "Properties",
                            "name": "Pressure",
                            "unit": "atm",
                            "value": 1
                        },
                        {
                            "group": "Electroneutrality Options",
                            "name": "ElectroNeutralityBalanceType",
                            "value": "DominantIon"
                        },
                        {
                            "group": "Calculation Options",
                            "name": "CalcType",
                            "value": "EquilCalcOnly"
                        },
                        {
                            "group": "Calculation Options",
                            "name": "CalcAlkalnity",
                            "value": False
                        },
                        {
                            "group": "Calculation Options",
                            "name": "AllowSolidsToForm",
                            "value": False
                        }
                    ],
                    'optionalProperties': {'scalingIndex': True,
                                           'scalingTendencies': True},
                    "unitSetInfo": {
                        "liq1_phs_comp": "ppm (mass)",
                        "solid_phs_comp": "ppm (mass)",
                        "vapor_phs_comp": "ppm (mass)",
                        "liq2_phs_comp": "ppm (mass)",
                        "combined_phs_comp": "ppm (mass)",
                        "pt": "psig",
                        "t": "°F",
                        "enthalpy": "J",
                        "density": "g/m3",
                        "mass": "ppm (mass)",
                        "vol": "L"
                    },
                    "molecularConversion": {
                        "option": "inflowRateBased"
                    }
                }

            }
        # # call the flash function
            result= oliapi.call("wateranalysis", chemistry_file_id, brine_input)
            with open("Data/result_stuio_dbs.json", 'w', encoding='utf-8') as f:
                json.dump(result, f, ensure_ascii=False, indent=4)

            SI.append(result['result']['additionalProperties']['scalingTendencies']['values']['CASO4.2H2O'])
            #print(f"case = {row}, ")

            myClient = pymongo.MongoClient("mongodb://localhost:27017/")
            myDB = myClient["myDB_NAWI"]
            myCollection_properties = myDB["result_properties"] 
            myCollection_mapping_table = myDB["result_map_table"] 
            try: 
                    insert_properties_status = myCollection_properties.insert_one(copy.deepcopy(result['result']['additionalProperties']['scalingTendencies']))
            except KeyError:
                print("Key error")

            data_map = {"data_mapping": {"properties_result_field": str( getattr(insert_properties_status, 'inserted_id', None)), "input_values": {"Na": float(df['Na'][row]),"Ca": float(df['Ca'][row]),"Mg": float(df['Mg'][row]),"SO4": float(df['SO4'][row]),"Cl": float(df['Cl'][row])}}}
            insert_datamap_status = myCollection_mapping_table.insert_one(data_map)
            quit() 
        #print(json.dumps(result, indent=2).encode('utf8'))
        print(SI)